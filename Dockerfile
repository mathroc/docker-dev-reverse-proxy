ARG TAG

FROM traefik:$TAG

RUN apk add --no-cache openssl

ADD entrypoint.sh /entrypoint.sh
ADD generate_certificates.sh /generate_certificates.sh